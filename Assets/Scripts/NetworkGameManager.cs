﻿/*
"Mistake the Getaway" Kevin Macleod(incomputech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Hitman" Kevin Macleod(incomputech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NetworkGameManager : MonoBehaviour
{
    public GameObject score;

    public GameObject[] spawnPoints;
    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(score.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas)
        {
            if (Input.GetButtonUp("Host")) GameObject.Find("NetworkManager").GetComponent<NetworkManagerCustome>().StartUpHost();
            if (Input.GetButtonUp("Join")) GameObject.Find("NetworkManager").GetComponent<NetworkManagerCustome>().JoinGame();
        }
    }
	public void endGame() {
		SceneManager.LoadScene("Scoreboard");	
	}
}