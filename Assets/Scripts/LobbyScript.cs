﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LobbyScript : MonoBehaviour {

	public Dropdown mode;
	public Dropdown levelSelect;
	public Selectable[] MenuList;
	public Button Play;
	public Image level;
	public int MenuCounter = 0;
	public const int MenuItems = 3;
	public bool inputEnabled;
	public bool isRunning;
	public string[] levels;
	public Sprite cross;
	public Sprite tdm;
	public Sprite[] screens;
	public string[] onlineLevels;
	public int selectedLevel;

	// Use this for initialization
	void Start () {
		EventSystem.current.SetSelectedGameObject (mode.gameObject);
		MenuList = new Selectable[MenuItems];
		MenuList [0] = mode;
		MenuList [1] = levelSelect;
		MenuList [2] = Play;
		inputEnabled = true;
		isRunning = false;
		screens = new Sprite[3];
		screens[0] = cross;
		screens [1] = tdm;
		screens[2] = cross;

		levels = new string[3];
		levels[0] = "Square";
		levels [1] = "TeamDeathMatch2";
		levels [2] = "Cross 2.1";

		onlineLevels = new string[2];
		onlineLevels[0] = "NetworkedCross 2.0";
		onlineLevels [1] = "NetworkedTeamDeathMatch";
	}

	// Update is called once per frame
	void Update () {

		level.gameObject.GetComponent<Image> ().sprite = screens[levelSelect.value]; 


		if (!inputEnabled) {
			if (!isRunning)
				StartCoroutine (wait());
		}
		float y = Input.GetAxis ("Jump");
		float z = Input.GetAxis ("Back");

		if (z == 1) {
			SceneManager.LoadScene ("Main Menu");
		}

		if (y == 1 && EventSystem.current.currentSelectedGameObject.name == "Play") {
			if (mode.value == 0) {
				SceneManager.LoadScene (levels[levelSelect.value]);
				if (levelSelect.value == 2)
					SceneManager.LoadScene ("Cross 2.1");
			}
			else {
				if (levelSelect.value < 2)
					SceneManager.LoadScene (onlineLevels[levelSelect.value]);
			}
		}
	}

	IEnumerator wait() {
		isRunning = true;
		yield return new WaitForSeconds(0.25f);
		inputEnabled = true;
		isRunning = false;
	}
}
	