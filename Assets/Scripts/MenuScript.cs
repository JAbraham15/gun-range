﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuScript : MonoBehaviour {

	public Button Multiplayer;
	public Button Quit;
	public Button[] MenuList;
	public int MenuCounter = 0;
	public const int MenuItems = 2;
	public bool inputEnabled;
	public bool isRunning;

	// Use this for initialization
	void Start () {
		EventSystem.current.SetSelectedGameObject (Multiplayer.gameObject);
		MenuList = new Button[MenuItems];
		MenuList [0] = Multiplayer;
		MenuList [1] = Quit;
		inputEnabled = true;
		isRunning = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!inputEnabled) {
			if (!isRunning)
				StartCoroutine (wait());
		}

		if (Input.GetButtonUp("Jump")) {
			if (EventSystem.current.currentSelectedGameObject.name == "Multiplayer")
				SceneManager.LoadScene ("Lobby");
			if (EventSystem.current.currentSelectedGameObject.name == "Quit")
				SceneManager.LoadScene("Credits");	
			if (EventSystem.current.currentSelectedGameObject.name == "Achievements") {
				SceneManager.LoadScene ("Achievements");
			}
		}
	}

	IEnumerator wait() {
		isRunning = true;
		yield return new WaitForSeconds(0.25f);
		inputEnabled = true;
		isRunning = false;
	}
}
