﻿/*
"Mistake the Getaway" Kevin Macleod(incomputech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Hitman" Kevin Macleod(incomputech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager2 : MonoBehaviour
{

	public Text TimeDisplay;

	public int p1Lives = 2;
	public int p2Lives = 2;
	public float timeLeft = 120.0f;


	public GameObject player1Prefab;
	private GameObject player1Spawned;

	public GameObject player2Prefab;
	private GameObject player2Spawned;

	public PlayerControl playerControl;
	public PlayerControl2 playerControl2;

	public Achievements achievements;

	public GameObject score;

	public GameObject[] spawnPoints;
	// Use this for initialization
	void Start()
	{
		player1Spawned = (GameObject)Instantiate(player1Prefab, spawnPoints[0].transform.position, spawnPoints[0].transform.rotation);
		player2Spawned = (GameObject)Instantiate(player2Prefab, spawnPoints[1].transform.position, spawnPoints[1].transform.rotation);
		playerControl = player1Spawned.GetComponent<PlayerControl>();
		playerControl2 = player2Spawned.GetComponent<PlayerControl2>();
		achievements = GetComponent<Achievements> ();
		achievements.init();
		DontDestroyOnLoad (score.gameObject);
	}

	// Update is called once per frame
	void Update()
	{
		/*int minutes = (int)Mathf.Floor(timeLeft/60.0f);
		int seconds = (int)(timeLeft-60*minutes);
		if (minutes > 0 && seconds >= 10) 
			TimeDisplay.text = minutes.ToString () + ":" + seconds.ToString();
		else if (minutes > 0 && seconds < 10)
			TimeDisplay.text = minutes.ToString () + ":0" + seconds.ToString();
		else if (minutes == 0 && seconds >= 10)
			TimeDisplay.text = "0" + minutes.ToString () + ":" + seconds.ToString();
		else if (minutes == 0 && seconds < 10)
			TimeDisplay.text = "0" + minutes.ToString () + ":0" + seconds.ToString();

		timeLeft -= Time.deltaTime;*/


		if (p1Lives <= 0|| p2Lives <= 0) {
			SceneManager.LoadScene ("Scoreboard");
		}




		if (playerControl.health <= 0) {
			p1Lives--;
			achievements.increment ((int)Achievements.Achi.die5);
			achievements.killstreak ();
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer2Kills (1);
				scoreBoard.incrementplayer1Deaths (1);
			}
			playerControl.health = 100;
			playerControl.GetComponent<Health> ().resetHealth ();
			player1Spawned.transform.position = spawnPoints [0].transform.position;
			playerControl.onBackground = true;
			playerControl.canHeal = true;
		}

		if (playerControl2.health <= 0) {
			p2Lives--;
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer1Kills (1);
				scoreBoard.incrementplayer2Deaths (1);
			}
			playerControl2.health = 100;
			playerControl2.GetComponent<Health> ().resetHealth ();
			player2Spawned.transform.position = spawnPoints [1].transform.position;
			playerControl2.onBackground = true;
			playerControl2.canHeal = true;
		}
	}
}