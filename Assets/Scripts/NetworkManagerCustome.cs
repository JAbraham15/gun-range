﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//loosely based on https://www.youtube.com/watch?v=9w2kwGDZ6wM

public class NetworkManagerCustome : NetworkManager {

	public void StartUpHost(){
		NetworkManager.singleton.StopHost ();
		SetPort ();
		NetworkManager.singleton.serverBindToIP = true;
		//NetworkManager.singleton.serverBindAddress = "localhost";//Network.player.ipAddress;
		SetIPAddress();
		NetworkManager.singleton.StartHost ();
        Destroy(GameObject.Find("Canvas"));

	}

	void SetPort(){
		NetworkManager.singleton.networkPort = 7779;
	}

	public void JoinGame(){

		SetIPAddress ();
		SetPort ();
		NetworkManager.singleton.StartClient ();
        Destroy(GameObject.Find("Canvas"));
    }

	void SetIPAddress(){
		string ipAddress = GameObject.Find ("InputFieldIPAddress").transform.FindChild ("Text").GetComponent<Text> ().text;
		NetworkManager.singleton.networkAddress = ipAddress;
	}
		

	void OnEnable(){
		SceneManager.sceneLoaded += SetUpMenuSceneButtons;
	}


	public void DisconnectFromNetwork(){
		NetworkManager.singleton.StopClient();
		NetworkManager.singleton.StopHost();
		NetworkManager.singleton.StopMatchMaker();
		Network.Disconnect();

	}

	void SetUpMenuSceneButtons(Scene scene, LoadSceneMode mode){
		Debug.Log ("Menu Buttons");
		GameObject.Find ("StartHost").GetComponent<Button> ().onClick.RemoveAllListeners();
		GameObject.Find ("StartHost").GetComponent<Button> ().onClick.AddListener(StartUpHost);
		GameObject.Find ("JoinGame").GetComponent<Button> ().onClick.RemoveAllListeners();
		GameObject.Find ("JoinGame").GetComponent<Button> ().onClick.AddListener(JoinGame);
		GameObject.Find ("Disconnect").GetComponent<Button> ().onClick.RemoveAllListeners();
		GameObject.Find ("Disconnect").GetComponent<Button> ().onClick.AddListener (DisconnectFromNetwork);
       
	}

	void SetUpOtherButtons(){
		GameObject.Find ("ButtonDisconnect").GetComponent<Button> ().onClick.RemoveAllListeners();
		GameObject.Find ("ButtonDisconnect").GetComponent<Button> ().onClick.AddListener (NetworkManager.singleton.StopHost);

	}
}
