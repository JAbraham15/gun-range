﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{

	public Text TimeDisplay;

	public int p1Lives = 3;
	public int p2Lives = 3;
	public int p3Lives = 3;
	public int p4Lives = 3;
	public float timeLeft = 120.0f;


    public GameObject player1Prefab;
    private GameObject player1Spawned;

    public GameObject player2Prefab;
    private GameObject player2Spawned;

	public GameObject player3Prefab;
	private GameObject player3Spawned;

	public GameObject player4Prefab;
	private GameObject player4Spawned;

	public PlayerControl1Team1 playerControl;
    public PlayerControl2Team2 playerControl2;
	public PlayerControl3Team1 playerControl3;
	public PlayerControl4Team2 playerControl4;

	public Achievements achievements;

	public GameObject score;

    public GameObject[] spawnPoints;
    // Use this for initialization
    void Start()
    {
        player1Spawned = (GameObject)Instantiate(player1Prefab, spawnPoints[0].transform.position, spawnPoints[0].transform.rotation);
        player2Spawned = (GameObject)Instantiate(player2Prefab, spawnPoints[1].transform.position, spawnPoints[1].transform.rotation);
		player3Spawned = (GameObject)Instantiate(player3Prefab, spawnPoints[2].transform.position, spawnPoints[2].transform.rotation);
		player4Spawned = (GameObject)Instantiate(player4Prefab, spawnPoints[3].transform.position, spawnPoints[3].transform.rotation);
		playerControl = player1Spawned.GetComponent<PlayerControl1Team1>();
		playerControl2 = player2Spawned.GetComponent<PlayerControl2Team2>();
		playerControl3 = player3Spawned.GetComponent<PlayerControl3Team1>();
		playerControl4 = player4Spawned.GetComponent<PlayerControl4Team2>();
		achievements = GetComponent<Achievements> ();
		achievements.init();
		DontDestroyOnLoad (score.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
		/*int minutes = (int)Mathf.Floor(timeLeft/60.0f);
		int seconds = (int)(timeLeft-60*minutes);
		if (minutes > 0 && seconds >= 10) 
			TimeDisplay.text = minutes.ToString () + ":" + seconds.ToString();
		else if (minutes > 0 && seconds < 10)
			TimeDisplay.text = minutes.ToString () + ":0" + seconds.ToString();
		else if (minutes == 0 && seconds >= 10)
			TimeDisplay.text = "0" + minutes.ToString () + ":" + seconds.ToString();
		else if (minutes == 0 && seconds < 10)
			TimeDisplay.text = "0" + minutes.ToString () + ":0" + seconds.ToString();

		timeLeft -= Time.deltaTime;*/
		if (p2Lives <= 0 && p4Lives <= 0) {
			achievements.increment ((int)Achievements.Achi.win5);
		}

		if ((p1Lives <= 0 && p3Lives <= 0) || (p2Lives <= 0 && p4Lives <= 0)) {
			SceneManager.LoadScene ("Scoreboard");
		}

		if (p1Lives <= 0)
			Destroy (player1Spawned.gameObject);
		if (p2Lives <= 0)
			Destroy (player2Spawned.gameObject);
		if (p3Lives <= 0)
			Destroy (player3Spawned.gameObject);
		if (p4Lives <= 0)
			Destroy (player4Spawned.gameObject);

		if (playerControl.health <= 0 && p1Lives > 0) {
			p1Lives--;
			achievements.increment ((int)Achievements.Achi.die5);
			achievements.killstreak ();
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer2Kills (1);
				scoreBoard.incrementplayer1Deaths (1);
			}
			playerControl.health = 100;
			playerControl.GetComponent<Health> ().resetHealth ();
			player1Spawned.transform.position = spawnPoints [0].transform.position;
			playerControl.onBackground = true;
			playerControl.canHeal = true;
		}

		if (playerControl2.health <= 0 && p2Lives > 0) {
			p2Lives--;
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer1Kills (1);
				scoreBoard.incrementplayer2Deaths (1);
			}
			playerControl2.health = 100;
			playerControl2.GetComponent<Health> ().resetHealth ();
			player2Spawned.transform.position = spawnPoints [1].transform.position;
			playerControl2.onBackground = true;
			playerControl2.canHeal = true;
		}
		if (playerControl3.health <= 0 && p3Lives > 0) {
			p3Lives--;
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer2Kills (1);
				scoreBoard.incrementplayer1Deaths (1);
			}
			playerControl3.health = 100;
			playerControl3.GetComponent<Health> ().resetHealth ();
			player3Spawned.transform.position = spawnPoints [2].transform.position;
			playerControl3.onBackground = true;
			playerControl3.canHeal = true;
		}

		if (playerControl4.health <= 0 && p4Lives > 0) {
			p4Lives--;
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer1Kills (1);
				scoreBoard.incrementplayer2Deaths (1);
			}
			playerControl4.health = 100;
			playerControl4.GetComponent<Health> ().resetHealth ();
			player4Spawned.transform.position = spawnPoints [3].transform.position;
			playerControl4.onBackground = true;
			playerControl4.canHeal = true;
		}
    }
}