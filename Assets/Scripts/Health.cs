﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {
	public const float maxHealth = 100;
    [SyncVar]
    public float currentHealth = maxHealth;
	public Slider healthbar;
	public Image cross;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		healthbar.value = currentHealth;
	}

    public void takeDamage(float amount) {
		currentHealth -= amount;
	}
	public void heal(float amount){
		currentHealth += amount;
		if (currentHealth > maxHealth)
			currentHealth = maxHealth;
	}
	public void resetHealth() {
		currentHealth = maxHealth;
	}

	public void removeCross() {
		cross.gameObject.SetActive (false);
	}

    public void activateCross()
    {
        cross.gameObject.SetActive(true);
    }
}
