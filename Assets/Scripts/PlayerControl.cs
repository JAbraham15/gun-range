﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{

	//Public variables
	public float MaxSpeed = 20;
	public float Acceleration = 64;
	public float JumpSpeed = 8;
	public float JumpDuration = 150;
	public float Gravity = -50;
	public float health = 100;
	public float rifleDamage = 25;
	public float sniperDamage = 90;
	public float smgDamage = 15;
	public float shotgunDamage = 15;

	//Input variables
	private float horizontal;
	private float vertical;
	private float jumpInput;
	private float fireTrigger;
	private float dpadHorizontal;
	private float dpadVertical;
	private bool swap = false;
	private bool heal = false;

	//Internal variables
	private float oldAcc = 0;
	private float oldSpd = 0;
	private bool onTheGround;
	private float jmpDuration;
	private bool jumping = false;
	private float jumpCounter = 0;
	private float jumpCooldown = 0.2F;
	private float movement_Anim;
	private bool rifleFired = false;
	private bool sniperFired = false;
	public float bulletsPerSec = 0.5F;
	public float sniperPerSec = 1.5F;
	private bool smgFired = false;
	private bool shotgunFired = false;
	public float smgPerSec = 0.2F;
	public float shotgunPerSec = 1.0F;
	private float rifleCooldown = 0;
	private float sniperCooldown = 0;
	private float smgCooldown = 0;
	private float shotgunCooldown = 0;
	public bool onBackground = true;
	private bool blockLane = false;
	public bool canHeal = true;
	private float laneCooldown = 1;
	private float laneChange = 1.0F;
	private Vector3 targetPosition;
	private bool laneSwapping;
	private float swapDuration = .08F;
	private float swapTime = 0.0F;

	private enum Weapons
	{
		rifle,
		sniper,
		smg,
		shotgun
	}

	int fireMode = 0;
	int oldWeapon = 1;

	Rigidbody rigid;
	Animator anim;
	LayerMask layerMask;
	Transform modelTrans;
	public AudioSource audioSource;
	public AudioSource audio2;
	public Achievements achievements;

	public GameObject rifleBullet;
	public GameObject sniperBullet;
	public GameObject smgBullet;
	public GameObject shotgunBullet;
	public GameObject spawnPoint;
	public Transform shoulderTrans;
	public Transform rightShoulder;
	public Vector3 lookPos;
	public Vector3 stickAim;
	public Vector3 rStick;
	GameObject rsp;
	public Sprite assaultSprite;
	public Sprite sniperSprite;
	public Sprite smgSprite;
	public Sprite shotgunSprite;
	public Image gun;
	public Vector3 startPos;

	void Start ()
	{
		startPos = transform.position;
		laneSwapping = false;
		targetPosition = transform.position;
		gun.sprite = assaultSprite;
		oldAcc = Acceleration;
		oldSpd = MaxSpeed;
		Physics.gravity = new Vector3 (0, Gravity, 0);
		lookPos = new Vector3 (Screen.width, 0);
		rigid = GetComponent<Rigidbody> ();
		SetupAnimator ();

		layerMask = ~(1 << 8);

		rsp = new GameObject ();
		rsp.name = transform.root.name + " Right Shoulder IK Helper";
		achievements = GetComponent<Achievements> ();
		achievements.init ();
	}

	void FixedUpdate ()
	{
		InputHandler ();
		UpdateRigidbodyValues ();
		MovementHandler ();
		HandleRotation ();
		HandleAimingPos ();
		HandleAnimations ();
		HandleShoulder ();
	}

	void InputHandler ()
	{
		horizontal = Input.GetAxis ("Horizontal");
		vertical = Input.GetAxisRaw ("Vertical");
		jumpInput = Input.GetAxis ("Jump");
		rStick.x = Input.GetAxis ("Right Joystick X");
		rStick.y = Input.GetAxis ("Right Joystick Y");
		fireTrigger = Input.GetAxisRaw ("Fire1");
		dpadHorizontal = Input.GetAxis ("Dpad Horizontal");
		dpadVertical = Input.GetAxis ("Dpad Vertical");
		swap = Input.GetButtonUp ("Swap");
		heal = Input.GetButtonUp ("Heal");
	}

	void MovementHandler ()
	{
		if (canHeal && heal) {
			canHeal = false;
			var Playerhealth = GetComponent<Health> ();
			if (Playerhealth) {
				achievements.increment ((int)Achievements.Achi.heal5);
				PlayerPrefs.SetInt ("Git gud", PlayerPrefs.GetInt ("Git gud") + 1);
				Playerhealth.removeCross ();
				Playerhealth.heal (50);
				health += 50;
				if (health > 100)health = 100;
			}
		}

		if (dpadHorizontal == -1 && fireMode != (int)Weapons.rifle) {
			if (fireMode != oldWeapon)
				oldWeapon = fireMode;
			gun.sprite = assaultSprite;
			fireMode = (int)Weapons.rifle;
			rifleFired = true;
			rifleCooldown = Time.deltaTime;
		}
		if (dpadHorizontal == 1 && fireMode != (int)Weapons.sniper) {
			if (fireMode != oldWeapon)
				oldWeapon = fireMode;
			gun.sprite = sniperSprite;
			fireMode = (int)Weapons.sniper;
			sniperFired = true;
			sniperCooldown = Time.deltaTime;
		}
		if (dpadVertical == -1 && fireMode != (int)Weapons.smg) {
			if (fireMode != oldWeapon)
				oldWeapon = fireMode;
			gun.sprite = smgSprite;
			fireMode = (int)Weapons.smg;
			smgFired = true;
			smgCooldown = Time.deltaTime;
		}
		if (dpadVertical == 1 && fireMode != (int)Weapons.shotgun) {
			if (fireMode != oldWeapon)
				oldWeapon = fireMode;
			gun.sprite = shotgunSprite;
			fireMode = (int)Weapons.shotgun;
			shotgunFired = true;
			shotgunCooldown = Time.deltaTime;
		}
		if (dpadHorizontal == 0 && dpadVertical == 0 && swap) {
			int temp = fireMode;
			fireMode = oldWeapon;
			if (oldWeapon == 0) {
				rifleFired = true;
				rifleCooldown = Time.deltaTime;
			}
			if (oldWeapon == 1) {
				sniperFired = true;
				sniperCooldown = Time.deltaTime;
			}
			if (oldWeapon == 2) {
				smgFired = true;
				smgCooldown = Time.deltaTime;
			}
			if (oldWeapon == 3) {
				shotgunFired = true;
				shotgunCooldown = Time.deltaTime;
			}
			oldWeapon = temp;
		}
		if (onTheGround) {
			jumping = false;
			jumpCounter += Time.deltaTime;
		}
		onTheGround = isOnGround ();
		if (this.transform.position.y == -29.5) {
			this.Acceleration = 200;
			this.MaxSpeed = oldSpd * 2;
		} else {
			this.Acceleration = oldAcc;
			this.MaxSpeed = oldSpd;
		}
		if (horizontal < -0.1f) {
			if (rigid.velocity.x > -this.MaxSpeed) {
				rigid.AddForce (new Vector3 (-this.Acceleration, 0, 0));
			} else {
				rigid.velocity = new Vector3 (-this.MaxSpeed, rigid.velocity.y, 0);
			}
		} else if (horizontal > 0.1f) {
			if (rigid.velocity.x < this.MaxSpeed) {
				rigid.AddForce (new Vector3 (this.Acceleration, 0, 0));
			} else {
				rigid.velocity = new Vector3 (this.MaxSpeed, rigid.velocity.y, 0);
			}
		}

		laneCooldown += Time.deltaTime;
		if (!blockLane && onTheGround) {
			if (!onBackground && vertical == 1) {
				swapTime = 0.0f;
				targetPosition = new Vector3 (transform.position.x, transform.position.y, startPos.z);
				onBackground = true;
				laneSwapping = true;
			} else if (onBackground && vertical == -1) {
				swapTime = 0.0f;
				targetPosition = new Vector3 (transform.position.x, transform.position.y, startPos.z - 5);
				onBackground = false;
				laneSwapping = true;
			}
		}

		if (Vector3.Distance (targetPosition, transform.position) > 0.2f && laneSwapping) {
			swapTime += Time.deltaTime;
			transform.position = Vector3.Lerp (transform.position, targetPosition, .2f);
		}
		if (swapTime > swapDuration) {
			laneSwapping = false;
			transform.position = new Vector3 (transform.position.x, transform.position.y, targetPosition.z);
		}
		blockLane = false;

		if (jumpInput > 0.1f && jumpCounter > jumpCooldown) {
			if (!jumping) {
				jumpCounter = 0;
				jumping = true;
				rigid.velocity = new Vector3 (rigid.velocity.x, this.JumpSpeed, 0);
			}
		}

		if (jumping) {
			rigid.AddForce (new Vector3 (0, -20, 0));
		}

		if (rifleCooldown >= bulletsPerSec) {
			rifleCooldown = 0;
			rifleFired = false;
		}
		if (sniperCooldown >= sniperPerSec) {
			sniperCooldown = 0;
			sniperFired = false;
		}
		if (smgCooldown >= smgPerSec) {
			smgCooldown = 0;
			smgFired = false;
		}
		if (shotgunCooldown >= shotgunPerSec) {
			shotgunCooldown = 0;
			shotgunFired = false;
		}
		if (fireTrigger > 0 && rifleCooldown == 0 && fireMode == 0) {
			fire ();
			rifleFired = true;
		}
		if (fireTrigger > 0 && sniperCooldown == 0 && fireMode == 1) {
			fire ();
			sniperFired = true;
		}
		if (fireTrigger > 0 && smgCooldown == 0 && fireMode == 2) {
			fire ();
			smgFired = true;
		}
		if (fireTrigger > 0 && shotgunCooldown == 0 && fireMode == 3) {
			fire ();
			shotgunFired = true;
		}
		if (rifleFired) {
			rifleCooldown += Time.deltaTime;
		}
		if (sniperFired) {
			sniperCooldown += Time.deltaTime;
		}
		if (smgFired) {
			smgCooldown += Time.deltaTime;
		}
		if (shotgunFired) {
			shotgunCooldown += Time.deltaTime;
		}
	}

	void HandleRotation ()
	{
		Vector3 directionToLook = lookPos - transform.position;
		directionToLook.y = 0;
		Quaternion targetRotation = Quaternion.LookRotation (directionToLook);

		//Debug.Log(lookPos.x + " " + transform.position.x);

		transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, Time.deltaTime * 15);
	}

	void HandleAimingPos ()
	{
		float x = rStick.x * Screen.width / 2f;// + Screen.width / 2f;
		float y = rStick.y * Screen.height / 2f;// + Screen.height / 2f;
		if (x != 0 || y != 0) {
			stickAim = Vector3.Normalize (new Vector3 (x, y));
			stickAim = new Vector3 (x, y);
		}
		//Vector3 stickToScreenSpace = new Vector3 (x, y);
		//Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		//Ray ray = Camera.main.ScreenPointToRay(stickAim);
		//RaycastHit hit;

		//if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		//{
		//    Vector3 lookP = hit.point;
		//    lookP.z = transform.position.z;
		//    lookPos = lookP;
		//}
		stickAim.z = transform.position.z;
		lookPos = stickAim;
	}

	void HandleAnimations ()
	{
		anim.SetBool ("OnAir", !onTheGround);
        
		float animValue = horizontal;

		if (lookPos.x < transform.position.x) {
			animValue = -animValue;
		}

		anim.SetFloat ("Movement", animValue, .1f, Time.deltaTime);
	}

	void HandleShoulder ()
	{
		shoulderTrans.LookAt (lookPos);

		Vector3 rightShoulderPos = rightShoulder.TransformPoint (Vector3.zero);
		rsp.transform.position = rightShoulderPos;
		rsp.transform.parent = transform;

		shoulderTrans.position = rsp.transform.position;
	}

	void UpdateRigidbodyValues ()
	{
		if (onTheGround) {
			rigid.drag = 4;  
		} else {
			rigid.drag = 4;
		}
	}

	private bool isOnGround ()
	{
		bool retVal = false;
		float lengthToSearch = 1.5f;    

		Vector3 lineStart = transform.position + Vector3.up;

		Vector3 vectorToSearch = -Vector3.up;

		RaycastHit hit;

		if (Physics.Raycast (lineStart, vectorToSearch, out hit, lengthToSearch, layerMask)) {
			retVal = true;
		}

		return retVal;
	}

	void SetupAnimator ()
	{
		// this is a ref to the animator component on the root.
		anim = GetComponent<Animator> ();

		// we use avatar from a child animator component if present
		// this is to enable easy swapping of the character model as a child node
		foreach (var childAnimator in GetComponentsInChildren<Animator>()) {
			if (childAnimator != anim) {
				anim.avatar = childAnimator.avatar;
				modelTrans = childAnimator.transform;
				Destroy (childAnimator);
				break; //if you find the first animator, stop searching
			}
		}
	}

	void fire ()
	{
		if (fireMode == 0) {
			audioSource.Play ();
			GameObject spawnedBullet = (GameObject)Instantiate (rifleBullet, spawnPoint.transform.position, spawnPoint.transform.rotation);
			spawnedBullet.name = "P1RifleBullet";
			Physics.IgnoreCollision (spawnedBullet.GetComponent<Collider> (), GetComponent<Collider> ());
			spawnedBullet.transform.Rotate (0, 90, 0);
			spawnedBullet.GetComponent<Rigidbody> ().velocity = Vector3.Normalize (stickAim) * 100;
			Destroy (spawnedBullet, 4);
		}
		if (fireMode == 1) {
			audioSource.Play ();
			GameObject spawnedBullet = (GameObject)Instantiate (sniperBullet, spawnPoint.transform.position, spawnPoint.transform.rotation);
			spawnedBullet.name = "P1SniperBullet";
			Physics.IgnoreCollision (spawnedBullet.GetComponent<Collider> (), GetComponent<Collider> ());
			spawnedBullet.transform.Rotate (0, 90, 0);
			spawnedBullet.GetComponent<Rigidbody> ().velocity = Vector3.Normalize (stickAim) * 100;
			Destroy (spawnedBullet, 4);
		}
		if (fireMode == 2) {
			audioSource.Play ();
			GameObject spawnedBullet = (GameObject)Instantiate (smgBullet, spawnPoint.transform.position, spawnPoint.transform.rotation);
			spawnedBullet.name = "P1SmgBullet";
			Physics.IgnoreCollision (spawnedBullet.GetComponent<Collider> (), GetComponent<Collider> ());
			spawnedBullet.transform.Rotate (0, 90, 0);
			spawnedBullet.GetComponent<Rigidbody> ().velocity = Vector3.Normalize (stickAim) * 100;
			Destroy (spawnedBullet, 4);
		}
		if (fireMode == 3) {
			audioSource.Play ();

			GameObject spawnedBullet = (GameObject)Instantiate (shotgunBullet, spawnPoint.transform.position, spawnPoint.transform.rotation);
			spawnedBullet.name = "P1ShotgunBullet";
			Physics.IgnoreCollision (spawnedBullet.GetComponent<Collider> (), GetComponent<Collider> ());
			spawnedBullet.transform.Rotate (0, 90, 0);
			spawnedBullet.GetComponent<Rigidbody> ().velocity = Vector3.Normalize (stickAim) * 100;
			Destroy (spawnedBullet, 4);

			var randomNumberY = Random.Range(1,15);
			var pos = new Vector3 (spawnPoint.transform.position.x, spawnPoint.transform.position.y, spawnPoint.transform.position.z);
			GameObject bullet = (GameObject)Instantiate (shotgunBullet, pos, spawnPoint.transform.rotation);
			bullet.name = "P1ShotgunBullet";
			Physics.IgnoreCollision (bullet.GetComponent<Collider> (), spawnedBullet.GetComponent<Collider> ());
			Physics.IgnoreCollision (bullet.GetComponent<Collider> (), GetComponent<Collider> ());
			bullet.transform.Rotate(randomNumberY, 0, 0);
			bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * 100;
			Destroy (bullet, 4);

			var randomNumberX = Random.Range(-15,-1);
			pos = new Vector3 (spawnPoint.transform.position.x, spawnPoint.transform.position.y, spawnPoint.transform.position.z);
			GameObject bullet1 = (GameObject)Instantiate (shotgunBullet, pos, spawnPoint.transform.rotation);
			bullet1.name = "P1ShotgunBullet";
			Physics.IgnoreCollision (bullet1.GetComponent<Collider> (), GetComponent<Collider> ());
			Physics.IgnoreCollision (bullet1.GetComponent<Collider> (), spawnedBullet.GetComponent<Collider> ());
			Physics.IgnoreCollision (bullet1.GetComponent<Collider> (), bullet.GetComponent<Collider> ());
			Physics.IgnoreCollision (spawnedBullet.GetComponent<Collider> (), bullet.GetComponent<Collider> ());
			Physics.IgnoreCollision (bullet.GetComponent<Collider> (), bullet1.GetComponent<Collider> ());
			Physics.IgnoreCollision (spawnedBullet.GetComponent<Collider> (), bullet1.GetComponent<Collider> ());
			bullet1.transform.Rotate(randomNumberX, 0, 0);
			bullet1.GetComponent<Rigidbody> ().velocity = bullet1.transform.forward * 100;
			Destroy (bullet1, 4);
		}
	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.gameObject.name.Contains ("RifleBullet")) {
			Destroy (collision.gameObject);
			var Playerhealth = GetComponent<Health> ();
			if (Playerhealth) {
				Playerhealth.takeDamage (rifleDamage);
				health -= rifleDamage;
				if(health<=0)
					audio2.Play ();
			}
		}
		if (collision.gameObject.name.Contains ("SniperBullet")) {
			Destroy (collision.gameObject);
			var Playerhealth = GetComponent<Health> ();
			if (Playerhealth) {
				Playerhealth.takeDamage (sniperDamage);
				health -= sniperDamage;
				if(health<=0)
					audio2.Play ();
			}
		}
		if (collision.gameObject.name.Contains ("SmgBullet")) {
			Destroy (collision.gameObject);
			var Playerhealth = GetComponent<Health> ();
			if (Playerhealth) {
				Playerhealth.takeDamage (smgDamage);
				health -= smgDamage;
			}
		}
		if (collision.gameObject.name.Contains ("ShotgunBullet")) {
			Destroy (collision.gameObject);
			var Playerhealth = GetComponent<Health> ();
			if (Playerhealth) {
				Playerhealth.takeDamage (shotgunDamage);
				health -= shotgunDamage;
				if(health<=0)
					audio2.Play ();
			}
		}
	}

	void OnTriggerStay (Collider c)
	{
		if (c.gameObject.name.Contains ("BlockMov")) {
			blockLane = true;
		}
	}
}
