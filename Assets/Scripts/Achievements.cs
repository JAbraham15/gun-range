﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Achievements : MonoBehaviour {
	public enum Achi
	{
		kill10,
		killRifle,
		killSniper,
		killSmg,
		killShotgun,
		die5,
		killstreak,
		win5,
		heal5,
		unlockAll
	}
	private string achievements;
	public int[,] progress;
	public string[] desc;
	public string[] title;
	private const int numAchievements = 10;
	private string saveTxt;

	public void init () {
		desc = new string[numAchievements];
		title = new string[numAchievements];
		progress = new int[numAchievements,2];
		//achievements = File.ReadAllText(@"achievements.txt");
		//string[] parse = achievements.Split (new char[]{' ', '\n'});
		//for (int i = 0; i < numAchievements; i++) {
		//	progress [i, 0] = Int32.Parse(parse [2*i]);
		//	progress [i, 1] = Int32.Parse(parse [2*i+1]);
		//}
		desc [0] = "Kill 10 enemies";				//Done
		desc [1] = "Kill 10 enemies with Rifle";	//Done
		desc [2] = "Kill 10 enemies with Sniper";	//Done
		desc [3] = "Kill 10 enemies with SMG";		//Done
		desc [4] = "Kill 10 enemies with Shotgun";	//Done
		desc [5] = "Die 5 times";					//Done
		desc [6] = "Killstreak of 5";				//Done
		desc [7] = "Win 5 games";					//Done
		desc [8] = "Heal 5 times";					//Done
		desc [9] = "Unlock all achievements";		//Done
		title [0] = "Just the start";
		title [1] = "How do I switch weapons?";
		title [2] = "Happy Camper";
		title [3] = "OP";
		title [4] = "Impossible";
		title [5] = "Git gud";
		title [6] = "MOAB";
		title [7] = "Meta evolved";
		title [8] = "Nerf pls";
		title [9] = "Pointless";
	}

	public void load(){
		//achievements = File.ReadAllText(@"achievements.txt");
		//string[] parse = achievements.Split (new char[]{' ', '\n'});
		//for (int i = 0; i < numAchievements; i++) {
		//	progress [i, 0] = Int32.Parse(parse [2*i]);
		//	progress [i, 1] = Int32.Parse(parse [2*i+1]);
		//}
	}
	public void save(){
		//File.Write
		//achievements = File.ReadAllText(@"achievements.txt");
		//string[] parse = achievements.Split (new char[]{' ', '\n'});
		saveTxt = "";
		bool allComplete = true;
		for (int i = 0; i < numAchievements; i++) {
			if (i != numAchievements - 1) {
				saveTxt += progress [i, 0].ToString () + " " + progress [i, 1] + "\r\n";
				if (progress [i, 0] < progress [i, 1]) {
					allComplete = false;
				}
			}
			else {
				if(allComplete)
					saveTxt += progress [i, 1].ToString () + " " + progress [i, 1];
				else
					saveTxt += progress [i, 0].ToString () + " " + progress [i, 1];
			}
		}
		//File.WriteAllText(@"achievements.txt", saveTxt);
	}
	public void increment(int i){
		load ();
		if (progress [i, 0] < progress [i, 1]) {
			progress [i, 0]++;
			save ();
		}
	}
	public void killstreak(){
		load ();
		if (progress [6, 0] < progress [6, 1]) {
			progress [6, 0] = 0;
		}
	}
	public bool locked(int i){
		load ();
		if (progress [i, 0] < progress [i, 1])
			return true;
		return false;
	}
}
