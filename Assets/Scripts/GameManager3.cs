﻿/*
"Mistake the Getaway" Kevin Macleod(incomputech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Hitman" Kevin Macleod(incomputech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager3 : MonoBehaviour
{

	public Text TimeDisplay;

	public int p1Lives = 2;
	public int p2Lives = 2;
	public int p3Lives = 2;
	public float timeLeft = 5.0f;

	Text text;

	public GameObject player1Prefab;
	private GameObject player1Spawned;

	public GameObject player2Prefab;
	private GameObject player2Spawned;

	public GameObject player3Prefab;
	private GameObject player3Spawned;

	public PlayerControl playerControl;
	public PlayerControl2 playerControl2;
	public PlayerControl3 playerControl3;

	public Achievements achievements;

	public GameObject score;

	public GameObject[] spawnPoints;
	// Use this for initialization
	void Start()
	{
		text = GameObject.Find ("Text").GetComponent<Text> ();
		player1Spawned = (GameObject)Instantiate(player1Prefab, spawnPoints[0].transform.position, spawnPoints[0].transform.rotation);
		player2Spawned = (GameObject)Instantiate(player2Prefab, spawnPoints[1].transform.position, spawnPoints[1].transform.rotation);
		player3Spawned = (GameObject)Instantiate(player3Prefab, spawnPoints[2].transform.position, spawnPoints[2].transform.rotation);
		playerControl = player1Spawned.GetComponent<PlayerControl>();
		playerControl2 = player2Spawned.GetComponent<PlayerControl2>();
		playerControl3 = player3Spawned.GetComponent<PlayerControl3>();
		achievements = GetComponent<Achievements> ();
		achievements.init();
		DontDestroyOnLoad (score.gameObject);
	}

	// Update is called once per frame
	void Update()
	{
		/*int minutes = (int)Mathf.Floor(timeLeft/60.0f);
		int seconds = (int)(timeLeft-60*minutes);
		if (minutes > 0 && seconds >= 10) 
			TimeDisplay.text = minutes.ToString () + ":" + seconds.ToString();
		else if (minutes > 0 && seconds < 10)
			TimeDisplay.text = minutes.ToString () + ":0" + seconds.ToString();
		else if (minutes == 0 && seconds >= 10)
			TimeDisplay.text = "0" + minutes.ToString () + ":" + seconds.ToString();
		else if (minutes == 0 && seconds < 10)
			TimeDisplay.text = "0" + minutes.ToString () + ":0" + seconds.ToString();

		timeLeft -= Time.deltaTime;*/


		if ((p1Lives <= 0 && p2Lives <= 0) || (p1Lives <= 0 && p3Lives <= 0) || (p2Lives <= 0 && p3Lives <= 0)) {
			timeLeft -= Time.deltaTime;
			if (p1Lives > 0) {
				text.text = "Player 1 wins!";
                timeLeft = 0;
			} 
			else if (p2Lives > 0) {
				text.text = "Player 2 wins!";
                timeLeft = 0;
            } 
			else if (p3Lives > 0) {
				text.text = "Player 3 wins!";
                timeLeft = 0;
            }
			if (timeLeft <= 0.0f) {
				SceneManager.LoadScene ("Scoreboard");
			}
		}

		if (p1Lives <= 0)
			Destroy (player1Spawned.gameObject);
		if (p2Lives <= 0)
			Destroy (player2Spawned.gameObject);
		if (p3Lives <= 0)
			Destroy (player3Spawned.gameObject);


		if (playerControl.health <= 0 && p1Lives > 0) {
			p1Lives--;
			achievements.increment ((int)Achievements.Achi.die5);
			achievements.killstreak ();
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer2Kills (1);
				scoreBoard.incrementplayer1Deaths (1);
			}
			playerControl.health = 100;
			playerControl.GetComponent<Health> ().resetHealth ();
			player1Spawned.transform.position = spawnPoints [0].transform.position;
			playerControl.onBackground = true;
			playerControl.canHeal = true;
		}

		if (playerControl2.health <= 0 && p2Lives > 0) {
			p2Lives--;
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer1Kills (1);
				scoreBoard.incrementplayer2Deaths (1);
			}
			playerControl2.health = 100;
			playerControl2.GetComponent<Health> ().resetHealth ();
			player2Spawned.transform.position = spawnPoints [1].transform.position;
			playerControl2.onBackground = true;
			playerControl2.canHeal = true;
		}
		if (playerControl3.health <= 0 && p3Lives > 0) {
			p3Lives--;
			var scoreBoard = score.GetComponent<ScoreScript> ();
			if (scoreBoard) {
				scoreBoard.incrementplayer2Kills (1);
				scoreBoard.incrementplayer1Deaths (1);
			}
			playerControl3.health = 100;
			playerControl3.GetComponent<Health> ().resetHealth ();
			player3Spawned.transform.position = spawnPoints [2].transform.position;
			playerControl3.onBackground = true;
			playerControl3.canHeal = true;
		}
	}
}