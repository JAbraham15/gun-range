﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScoreboardScript : MonoBehaviour {

	public Button Multiplayer;
	public Button Quit;
	public Button[] MenuList;
	public int MenuCounter = 0;
	public const int MenuItems = 2;
	public bool inputEnabled;
	public bool isRunning;
	public GameObject score;

	public Text P1Kills;
	public Text P2Kills;
	public Text P1Deaths;
	public Text P2Deaths;
	public Text P1Score;
	public Text P2Score;


	// Use this for initialization
	void Start () {
		MenuList = new Button[MenuItems];
		MenuList [0] = Multiplayer;
		MenuList [1] = Quit;
		inputEnabled = true;
		isRunning = false;

		score = GameObject.Find("Score");
	}

	// Update is called once per frame
	void Update () {
		if (!inputEnabled) {
			if (!isRunning)
				StartCoroutine (wait());
		}
		float x = Input.GetAxis ("Horizontal");
		if (x == 1 && inputEnabled == true) {
			MenuCounter++;
			inputEnabled = false;
		} 
		else if (x == -1 && inputEnabled == true) {
			MenuCounter--;
			inputEnabled = false;
		}
		if (MenuCounter < 0) {
			MenuCounter = 1;
		}
		MenuList [MenuCounter % MenuItems].Select ();
		float y = Input.GetAxis ("Jump");

		if (y == 1) {
			if (EventSystem.current.currentSelectedGameObject.name == "Multiplayer")
				SceneManager.LoadScene ("Main Menu");
			if (EventSystem.current.currentSelectedGameObject.name == "Quit")
				Application.Quit ();	
		}
			



	}

	IEnumerator wait() {
		isRunning = true;
		yield return new WaitForSeconds(0.25f);
		inputEnabled = true;
		isRunning = false;
	}
}
