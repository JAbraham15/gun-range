﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CreditsScript : MonoBehaviour {

	public bool inputEnabled;
	public bool isRunning;
	// Use this for initialization
	void Start () {
		inputEnabled = false;
		isRunning = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (!inputEnabled) {
			if (!isRunning)
				StartCoroutine (wait());
		}
		if (Input.GetButtonUp("Jump")) {
			SceneManager.LoadScene ("Main Menu");
		}
        if (Input.GetButtonUp("Back"))
        {
            SceneManager.LoadScene("Main Menu");
        }




    }

	IEnumerator wait() {
		isRunning = true;
		yield return new WaitForSeconds(0.25f);
		inputEnabled = true;
		isRunning = false;
	}
}