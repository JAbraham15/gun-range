﻿using UnityEngine;
using System.Collections;

public class ScoreScript : MonoBehaviour {

	public int player1Kills = 0;
	public int player2Kills = 0;
	public int player1Deaths = 0;
	public int player2Deaths = 0;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void incrementplayer1Kills(int amount) {
		player1Kills += amount;
	}
	public void incrementplayer2Kills(int amount) {
		player2Kills += amount;
	}
	public void incrementplayer1Deaths(int amount) {
		player1Deaths += amount;
	}
	public void incrementplayer2Deaths(int amount) {
		player2Deaths += amount;
	}
}
