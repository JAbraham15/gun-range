﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AchievementMenuScript : MonoBehaviour {
	
	public Button MainMenu;
	public bool inputEnabled;
	public bool isRunning;
	public GameObject score;
	public Text achievement1;
	public Text achievement2;
	public Text achievement3;
	public Text achievement4;
	public Text achievement5;
	public Text achievement6;
	public Text achievement7;
	public Text achievement8;
	public Text achievement9;
	public Text achievement10;
	public Text[] achievements;
	// Use this for initialization
	public string[] title;
	public string[] desc;
	public int[] threshhold;
	public int unlocked;
	void Start () {
		achievements = new Text[10];
		achievements [0] = achievement1;
		achievements [1] = achievement2;
		achievements [2] = achievement3;
		achievements [3] = achievement4;
		achievements [4] = achievement5;
		achievements [5] = achievement6;
		achievements [6] = achievement7;
		achievements [7] = achievement8;
		achievements [8] = achievement9;
		achievements [9] = achievement10;

		title = new string[10];
		desc = new string[10];
		threshhold = new int[10];
		desc [0] = "Kill 10 enemies";				//Done
		desc [1] = "Kill 10 enemies with Rifle";	//Done
		desc [2] = "Kill 10 enemies with Sniper";	//Done
		desc [3] = "Kill 10 enemies with SMG";		//Done
		desc [4] = "Kill 10 enemies with Shotgun";	//Done
		desc [5] = "Die 5 times";					//Done
		desc [6] = "Killstreak of 5";				//Done
		desc [7] = "Win 5 games";					//Done
		desc [8] = "Heal 5 times";					//Done
		desc [9] = "Unlock all achievements";		//Done
		title [0] = "Just the start";
		title [1] = "How do I switch weapons?";
		title [2] = "Happy Camper";
		title [3] = "OP";
		title [4] = "Impossible";
		title [5] = "Git gud";
		title [6] = "MOAB";
		title [7] = "Meta evolved";
		title [8] = "Nerf pls";
		title [9] = "Pointless";
		threshhold[0] = 10;
		threshhold[1] = 10;
		threshhold[2] = 10;
		threshhold[3] = 10;
		threshhold[4] = 10;
		threshhold[5] = 5;
		threshhold[6] = 5;
		threshhold[7] = 5;
		threshhold[8] = 5;
		threshhold[9] = 9;
		inputEnabled = false;

		for(int i = 0; i < 10; i++) {
			achievements[i].text = (i + 1).ToString() + ".\t";
			if (PlayerPrefs.GetInt (title [i]) >= threshhold [i]) {
				achievements [i].text += title [i] + " - ";
				PlayerPrefs.SetInt ("Pointless", PlayerPrefs.GetInt ("Pointless") + 1);
			} else {
				achievements [i].text += "??? - ";
			}
			achievements [i].text += desc [i] + ": " + PlayerPrefs.GetInt (title [i]).ToString () + "/" + threshhold [i];
		}
	}

	// Update is called once per frame
	void Update () {
		
		if (!inputEnabled) {
			if (!isRunning)
				StartCoroutine (wait());
		}
        if (Input.GetButtonUp("Jump"))
        {
            SceneManager.LoadScene("Main Menu");
        }
        if (Input.GetButtonUp("Back"))
        {
            SceneManager.LoadScene("Main Menu");
        }



    }

	IEnumerator wait() {
		isRunning = true;
		yield return new WaitForSeconds(0.25f);
		inputEnabled = true;
		isRunning = false;
	}
}
