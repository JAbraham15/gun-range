﻿using UnityEngine;
using System.Collections;

public class BillboardScript : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 v = new Vector3 (player.gameObject.transform.position.x,
			player.gameObject.transform.position.y, player.gameObject.transform.position.z + -300.0f);
		transform.LookAt (v);
	}
}
